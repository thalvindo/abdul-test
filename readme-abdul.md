At unknown laboratory, there was an evil and smart researcher. His name is Abdul. He is conducting some mysterious experiment about slime. He wanted to know which slime is the strongest by battling each other slime. He will send command from his computer to make slime battling each other. He also installed chip for each slime, so it can receive command from his computer.

The initial condition of the experiment is each slime has a chip ID and power 1. When 2 slime battle, the loser will merge into the winner. So the winner will have all the loser's power and chip ID.

There were 2 commands available, which are BATTLE and RANKING. He will receive feedback result for each command.

For BATTLE command (represented as 0), the feedback is ID of the winner.

For RANKING command (represented as 1), the feedback is 10 IDs of the strongest slime.

Slime with higher power and chip ID means stronger than others. If the slime is commanded to battle itself, it will return himself as the winner. If the merged loser is commanded to battle other slime, the winner will take over the command. If the remaining slime is less than 10 IDs, then it will show only the remaining slimes.

Abdul will send N commands to the computer, and he wants to see the feedback.

Input Format

The first line contains integer N, the number of commands.

Each of the next N lines contains 3 space-separated integers denoting command, ID of the first slime, and ID of the second slime.

Constraints

2 <= N <= 100.000
1 <= ID <= 100.000
RANKING command only called once at the end
Output Format

Each line contains the feedback from the command

Sample Input 0

12
0 1 2
0 3 4
0 5 6
0 7 8
0 9 10
0 11 12
0 13 14
0 15 16
0 17 18
0 19 20
0 21 22
1
Sample Output 0

2
4
6
8
10
12
14
16
18
20
22
22, 20, 18, 16, 14, 12, 10, 8, 6, 4
Explanation 0

There are 12 commands which would be send by Abdul.

The first command will make slime 1 and 2 battle each other.

At initial condition, they have same power, but the ID of slime 2 is higher than slime 1, so the winner is slime 2 and we send the feedback equals to 2.

The rest of the BATTLE command is same.

From 11 commands, we summarize 11 slime which has power of 2 which are 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22. The other slime which are not yet battle has a power of 1.

So, the ranking would be 22, 20, 18, 16, 14, 12, 10, 8, 6, 4

Sample Input 1

4
0 1 2
0 3 4
0 5 6
1
Sample Output 1

2
4
6
6, 4, 2, 100000, 99999, 99998, 99997, 99996, 99995, 99994
Explanation 1

There are 4 commands which would be send by Abdul.

From 3 commands, we summarize 3 slime which has power of 2 which are 2, 4, 6. The other slime which are not yet battle has a power of 1.

So, the ranking would be 6, 4, 2, 100000, 99999, 99998, 99997, 99996, 99995, 99994